#!/usr/bin/env bash

echo "Clean up from previous runs"
rm -rf ca client server

echo "Make the neeed directories"
mkdir ca server client

echo "Initialize ca serial"
echo 01 > ca/cert.srl

mkdir ca server client
echo "Generate ca private key"
openssl genrsa -out ca/key.pem

echo "Generate ca certificate"
openssl \
    req \
    -new \
    -x509 \
    -days 365 \
    -key ca/key.pem \
    -out ca/cert.pem \
    -subj "/C=US/ST=TX/L=San Antonio/O=Dreamer Labs/CN=ca.cloudflare.com/emailAddress=dev@dldev.xyz"

echo "Generate server private key"
openssl genrsa -out server/key.pem

echo "Generate server signing request"
openssl req \
    -new \
    -key server/key.pem \
    -out server/csr.csr \
    -subj "/C=US/ST=TX/L=San Antonio/O=Dreamer Labs/CN=api.cloudflare.com/emailAddress=dev@dldev.xyz"

echo "Generate server certificate"
openssl x509 -req -days 365 -in server/csr.csr -CA ca/cert.pem -CAkey ca/key.pem -out server/cert.pem

# echo "Create server/ca bundle"
# cat ca/cert.pem server/cert-single.pem > server/cert.pem

echo "Generate client key"
openssl genrsa -out client/key.pem

echo "Generate client signing request"
openssl req \
    -new \
    -key client/key.pem \
    -out client/csr.csr \
    -subj "/C=US/ST=TX/L=San Antonio/O=Dreamer Labs/CN=dnsrecords.dldev.xyz/emailAddress=dev@dldev.xyz"

echo "Generate client certificate"
openssl x509 -req -days 365 -in client/csr.csr -CA ca/cert.pem -CAkey ca/key.pem -out client/cert.pem 
